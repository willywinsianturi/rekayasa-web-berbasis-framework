<?php

namespace App\Http\Controllers;

use App\mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datamahasiswa = Mahasiswa::get(); // compact($var);
        return view('viewmahasiswa', ['datamahasiswa' => $datamahasiswa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Responsize:10
     */
    public function create()
    {
        return view('viewtambahmahasiswa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'nim' => 'required',
            'nama' => ['required'],
            'hp' => 'required',
        ]);

        /* cara 1  
        $mahasiswa = new mahasiswa;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->hp = $request->hp;
        $mahasiswa->save();
       */

        /* cara 2 

        Mahasiswa::create([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'hp' => $request->hp,
            
        ]);
*/
        $mahasiswa = new mahasiswa;
        $mahasiswa::create($request->all());



        return redirect('/add')->with('status', 'Data Mahasiswa berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(mahasiswa $mahasiswa)
    {
        return view('viewdetailmahasiswa', ['mahasiswa' => $mahasiswa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datamahasiswa = Mahasiswa::findorFail($id);
        // dd($datamahasiswa);
        return view('vieweditmahasiswa', ['mahasiswa' => $datamahasiswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mahasiswa $mahasiswa)
    {
        //
        $new_mahasiswa = Mahasiswa::findorFail($mahasiswa->id);
        $new_mahasiswa->nim = $request['nim'];
        $new_mahasiswa->nama = $request['nama'];
        $new_mahasiswa->hp = $request['hp'];

        // dd($new_mahasiswa);

        $new_mahasiswa->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(mahasiswa $mahasiswa)
    {
        //
        $data_mhs = Mahasiswa::findOrFail($mahasiswa->id);
        // dd($data_mhs);
        Mahasiswa::destroy($mahasiswa->id);
        return redirect('/');
    }
}
